import 'package:app/beer_response.dart';
import 'package:app/favorite.dart';
import 'package:app/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomePageState();
  }

}

class HomePageState extends State<HomePage> {

  List<BeerResponse> beerResponseList = new List();
  BeerResponse beerResponse;
  List<BeerResponse> favItems = new List();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        centerTitle: true,
        title: Text('Top Beers'),
        actions: <Widget>[
          GestureDetector(
            onTap: (){
              navigateToFav();
            },
              child: Icon(Icons.favorite))
        ],
      ),
      body: onGetBeers(),
    );
  }

  onGetBeers() {
    return FutureBuilder<List<dynamic>>(
        future: getBeerData("1"),
        builder: (context, snapshot) {
          print('onGetBeers');
          if(snapshot.hasData){
            if(beerResponseList!=null){
              beerResponseList.clear();
            }
            final beerResponse = BeerResponse.fromJson(
                snapshot.data.elementAt(1));
            print(beerResponse.name);
            sortBeerList(snapshot.data);
            return onShowBeers();
          }
          else{
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  sortBeerList(List<dynamic> dynamicResponse) {
    for (int i=0;i<dynamicResponse.length;i++) {
      final beerResponse = BeerResponse.fromJson(
          dynamicResponse.elementAt(i));
      beerResponseList.add(beerResponse);
    }
  }

  onShowBeers() {
    return Container(
      child: ListView.builder(
          itemCount: beerResponseList == null ? 0 : beerResponseList.length,
          itemBuilder: (context, position) {
            beerResponse = beerResponseList.elementAt(position);
            return Container(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Image.network(beerResponse.imageUrl,height: 50.0,width: 50.0,),
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.only(left: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(beerResponse.name,style: TextStyle(fontWeight: FontWeight.bold),),
                            Text(beerResponse.tagline)
                          ],
                        ),
                      ),
                    ),
                    onShowFavIcon(beerResponse)
                  ],
                ),
              ),
            );
          }),
    );
  }

  onShowFavIcon(BeerResponse beerResponse){
    if(getFavItem(beerResponse)!=null && getFavItem(beerResponse)){
      return GestureDetector(
          onTap: (){
            setState(() {
              onRemoveFavItem(beerResponse);
            });
          },
          child: Icon(Icons.favorite));
    }
    else{
      return GestureDetector(
        onTap: (){
          setState(() {
            favItems.add(beerResponse);
          });
        },
        child: Icon(Icons.favorite_border),
      );
    }
  }

  bool getFavItem(BeerResponse beerResponse){
    for(int i=0;i<favItems.length;i++){
      BeerResponse beerResponseFav = favItems.elementAt(i);
      if(beerResponseFav.name == beerResponse.name){
        return true;
      }
    }
  }

  onRemoveFavItem(BeerResponse beerResponse){
    for(int i=0;i<favItems.length;i++){
      BeerResponse beerResponseFav = favItems.elementAt(i);
      if(beerResponseFav.name == beerResponse.name){
        favItems.removeAt(i);
      }
    }
  }

  navigateToFav(){
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => Favorite(favItems)));
  }

}