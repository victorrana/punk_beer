import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:app/beer_response.dart';

const url = "https://api.punkapi.com/v2/beers?page=";

Future<List<dynamic>> getBeerData(String pageCount) async {
  final response =
      await http.get(url + pageCount, headers: {"Accept": "application/json"});

  print(response.body);

  if (response.statusCode == 200) {
    return json.decode(response.body.toString());
  } else {
    throw Exception('Failed to load post');
  }
}
