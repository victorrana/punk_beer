import 'package:app/beer_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Favorite extends StatefulWidget{

  List<BeerResponse> favList = new List();

  Favorite(this.favList);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FavoriteState(favList);
  }
  
}

class FavoriteState extends State<Favorite>{

  List<BeerResponse> favList = new List();
  BeerResponse beerResponse;

  FavoriteState(this.favList);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('Favorite'),
      ),
      body: onShowFavBeers(),
    );
  }

  onShowFavBeers() {
    print(favList.length);
    return Container(
      child: ListView.builder(
          itemCount: favList == null ? 0 : favList.length,
          itemBuilder: (context, position) {
            beerResponse = favList.elementAt(position);
            return Container(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Image.network(beerResponse.imageUrl,height: 50.0,width: 50.0,),
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.only(left: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(beerResponse.name,style: TextStyle(fontWeight: FontWeight.bold),),
                            Text(beerResponse.tagline)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }


}